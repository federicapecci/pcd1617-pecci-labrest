package pcd.lab11.mom.rabbitmq;
import com.rabbitmq.client.*;

import java.io.IOException;

public class Recv3 {

  private final static String QUEUE_NAME = "hello";

  public static void main(String[] argv) throws Exception {
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("localhost");
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

    Consumer consumer1 = new DefaultConsumer(channel) {
        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
            throws IOException {
          String message = new String(body, "UTF-8");
          System.out.println(" [x] Received 1 '" + message + "'");
          System.out.println(Thread.currentThread());
          // while (true){}
        }
      };
      Consumer consumer2 = new DefaultConsumer(channel) {
          @Override
          public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
              throws IOException {
            String message = new String(body, "UTF-8");
            System.out.println(" [x] Received 2 '" + message + "'");
            System.out.println(Thread.currentThread());
            while (true){}
          }
        };
    channel.basicConsume(QUEUE_NAME, true, consumer1);
    channel.basicConsume(QUEUE_NAME, true, consumer2);
    System.out.println(Thread.currentThread());
  }
}
