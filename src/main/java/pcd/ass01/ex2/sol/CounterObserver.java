package pcd.ass01.ex2.sol;

public interface CounterObserver {

	void counterUpdated(int value);
	
	void counterClosed();
}
