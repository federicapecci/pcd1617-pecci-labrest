package pcd.lab12.rest_services.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;

public class Client {

  // Convenience method so you can run it in your IDE
  public static void main(String[] args) {
	    Vertx vertx = Vertx.vertx();
	  	HttpClient client = vertx.createHttpClient();

	  	client.websocket(8080, "localhost", "/some-uri", ws -> {
	      ws.handler(data -> {
	        System.out.println("Received data " + data.toString("ISO-8859-1"));
	        try {
	        	Thread.sleep(500);
	        	ws.writeBinaryMessage(Buffer.buffer("ping"));
	        } catch (Exception ex){
	        	ex.printStackTrace();
	        }
	      });
	      ws.writeBinaryMessage(Buffer.buffer("ping"));
	    });
  }
}
