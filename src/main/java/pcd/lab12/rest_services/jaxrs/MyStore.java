package pcd.lab12.rest_services.jaxrs;

import java.util.HashMap;

public class MyStore {

	private HashMap<String,Object> map;
	static MyStore instance;

	public MyStore(){
		map = new HashMap<String,Object>();
	}
	
	public void store(String key, Object what){
		map.put(key, what);
	}

	public boolean isPresent(String key){
		return map.containsKey(key);
	}
	
	public Object get(String key){
		return map.get(key);
	}
	
	static public MyStore getInstance(){
		synchronized (MyStore.class){
			if (instance == null){
				instance = new MyStore();
			}
			return instance;
		}
	}
}
