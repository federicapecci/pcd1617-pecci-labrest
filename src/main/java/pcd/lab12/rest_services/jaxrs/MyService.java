package pcd.lab12.rest_services.jaxrs;

import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("myservice")
public class MyService {
	
	public MyService(){
        Logger.getGlobal().setLevel(Level.ALL);
	}

	@POST
	@Path("requests")
	@Produces(MediaType.APPLICATION_JSON)
	public String request(String msg) {
		try {
			JsonObject requestMsg = parseJSON(msg);
	        double input = requestMsg.getJsonNumber("input").doubleValue();
	        log("[POST NEW REQ] request - "+input);
	        // System.out.println(">> "+input);
			
	        String resId = MyController.getInstance().manageRequest(input);		
			log("request "+resId+" - result id: "+resId);

			JsonObject resultId = Json.createObjectBuilder()
	        		.add("resId", resId)
	        		.build();			
			
	        return resultId.toString();
		} catch (Exception ex){
			return "{ \"status\": \"error\" } ";
		}
	}

	@POST
	@Path("requestsWithHook")
	@Produces(MediaType.APPLICATION_JSON)
	public String requestWithHook(String msg) {
		try {
			JsonObject requestMsg = parseJSON(msg);
	        double input = requestMsg.getJsonNumber("input").doubleValue();
	        // System.out.println(">> "+input);

	        String uri = requestMsg.getString("resURI");
	        log("[POST NEW REQ WITH HOOK] request - "+input+" - res to create: "+uri);
	        
	        String resId = MyController.getInstance().manageRequest(input,uri);		
			log("request "+resId+" - result id: "+resId);

			JsonObject resultId = Json.createObjectBuilder()
	        		.add("resId", resId)
	        		.build();			
			
	        return resultId.toString();
		} catch (Exception ex){
			return "{ \"status\": \"error\" } ";
		}
	}
	
	
    @GET
    @Path("results/{resId}")
	@Produces(MediaType.APPLICATION_JSON)
    public String getResult(@PathParam("resId") String resId) {
    	try {
			log("[GET RES] result "+resId);    		
    		JsonObject res = (JsonObject) MyStore.getInstance().get(resId);
			log("[GET RES] in store: "+res);    		
    		if (res != null){
    			return res.toString();
    		} else {
    			return "{ \"status\": \"not available\" }"; 
    		}
    	} catch (Exception ex){
			return "{ \"status\": \"internal error\" }"; 
    	}
    }

    private JsonObject parseJSON(String s) throws Exception {
        JsonReader reader = Json.createReader(new StringReader(s));        
        JsonObject jsonObj = reader.readObject();         
        reader.close();
        return jsonObj;
    }
    
    private void log(String msg){
    	Logger.getGlobal().log(Level.INFO,msg);
    }
}