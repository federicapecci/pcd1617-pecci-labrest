package pcd.lab12.rest_services.jaxrs;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.client.Entity.json;

import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class MyConsumer {

	public static void main(String[] args) {
		Client client = ClientBuilder.newClient();
		WebTarget myRes = client.target("http://localhost:8080/myservice/requests");

		JsonObject msg = Json.createObjectBuilder()
        		.add("input", 16)
        		.build();			
		
		/* sync */
		Invocation req = myRes.request(MediaType.APPLICATION_JSON).buildPost(json(msg.toString()));
		String res = req.invoke(String.class);
		System.out.println(">> res: "+res);
	}
	
    private static JsonObject parseJSON(String s) throws Exception {
        JsonReader reader = Json.createReader(new StringReader(s));        
        JsonObject jsonObj = reader.readObject();         
        reader.close();
        return jsonObj;
    }
	

}
