package pcd.lab03.factorizer;

public interface FactorizerService {

	int[] getFactors(long n);

}
