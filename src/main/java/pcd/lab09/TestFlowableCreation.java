package pcd.lab09;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

public class TestFlowableCreation {

	public static void main(String[] args) throws Exception {

		Flowable<String> source = Flowable.create(emitter -> {		     
			System.out.println("CREATE-TH:"+Thread.currentThread().getName());

			new Thread(() -> {
				int i = 0;
				while (i<10){
					try {
						System.out.println("TH:"+Thread.currentThread().getName());
						emitter.onNext("-> "+i);
						Thread.sleep(100);
						i++;
					} catch (Exception ex){}
				}
			}).start();

		     //emitter.setCancellable(c::close);

		 }, BackpressureStrategy.BUFFER);

		//  Thread.sleep(1000);
		System.out.println("TH:"+Thread.currentThread().getName());
		
		source.subscribe((s) -> {
			System.out.println("TH:"+Thread.currentThread().getName());
			System.out.println(s); 
			// while (true){}
		});	
		
		Thread.sleep(10000);
	}

}
