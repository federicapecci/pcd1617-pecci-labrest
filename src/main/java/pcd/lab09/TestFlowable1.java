package pcd.lab09;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

public class TestFlowable1 {

	public static void main(String[] args) throws Exception {

		Flowable.fromCallable(() -> {
		    for (int i = 0; i < 10; i++){
		    	System.out.print(".");
			    Thread.sleep(250); //  imitate expensive computation
		    }
		    return "Done";
		})
		  .subscribeOn(Schedulers.io())
		  .observeOn(Schedulers.single())
		  .subscribe(System.out::println, Throwable::printStackTrace);

		/*
	    for (int i = 0; i < 20; i++){
	    	System.out.println();
		    Thread.sleep(500); //  imitate expensive computation
	    }*/
		
		// while(true){}
		Thread.sleep(10000); // <--- wait for the flow to finish		
	}

}
