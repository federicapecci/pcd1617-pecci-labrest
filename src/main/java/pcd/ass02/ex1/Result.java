package pcd.ass02.ex1;

public interface Result {
	
	boolean found();
	boolean isGreater();	
	boolean isLess();

}
