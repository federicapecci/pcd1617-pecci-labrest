package pcd.lab04.gui3_deadlock;

public interface ModelObserver {

	void modelUpdated(MyModel model);
}
