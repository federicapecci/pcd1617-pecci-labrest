package main

import (
  "fmt"
)

func pinger(n uint64, in chan uint64, out chan uint64, done chan bool) {
  for i := 0; ; i++ {
    value := <- in
    next := value + 1
    fmt.Printf("ping! %d \n",next)    
    if next > n {
      fmt.Printf("end pinger %d \n",next)    
      done <- true
      break
    } else {
      out <- next
    }
  }
}

func ponger(n uint64, in chan uint64, out chan uint64, done chan bool) {
  for i := 0; ; i++ {
    value := <- in
    next := value + 1
    fmt.Printf("pong! %d \n",next)    
    out <- next
    if next > n {
      fmt.Printf("end ponger %d \n",next)    
      done <- true
      break
    }
  }
}


func main() {
  var pingch chan uint64 = make(chan uint64)
  var pongch chan uint64 = make(chan uint64)
  var done chan bool = make(chan bool)

  var n uint64 = 1000

  go pinger(n, pongch, pingch, done)
  go ponger(n, pingch, pongch, done)

  pongch <- 1
  
  <- done
  <- done

  fmt.Println("done.")
}